function fail() {
echo "*** webrtc build failed"
exit 1
}

function fetch() {
    echo "-- fetching webrtc"
    gclient config --name trunk ssh://git@git.ringcentral.com/oleg.golosovskiy/webrtc.git@ea76b3c
    echo "target_os = ['mac']" >> .gclient
    gclient sync
     
    sed -i "" '$d' .gclient
    echo "target_os = ['ios', 'mac']" >> .gclient
    gclient sync
    echo "-- webrtc has been sucessfully fetched"

    pushd trunk/third_party/libvpx
    svn revert source/libvpx/vp8/decoder/decodeframe.c
    svn revert source/libvpx/vp8/encoder/pickinter.c
    svn revert libvpx.gyp
    patch -N -p0 -i ../../../libvpxfix.diff || fail
    popd

    pushd trunk/build
    svn revert common.gypi
    patch -N -p0 -i ../../build.diff || fail    
    popd

    pushd trunk/third_party/nss
    svn revert nss.gyp
    patch -N -p0 -i ../../../nss.diff || fail
    popd

    pushd trunk
    git checkout -- webrtc/base/base.gyp
    git apply ../webrtc_base.diff || fail
    popd

    pushd trunk/third_party/openssl
    svn revert openssl.gyp
    patch -N -p0 -i ../../../openssl.diff || fail
    popd
}

unpackAndCombine()
{
	LIB_PATH=$1
    AR_UTILITY=$2
    OUT_LIB=$3

	LIB_DIR=$(dirname $LIB_PATH)
	NAME=$(basename $LIB_PATH)
	OBJ_PATH=$LIB_DIR/OBJECT/$NAME
    echo "Unpacking $NAME from $LIB_PATH"

    mkdir -p $OBJ_PATH || fail
    rm -f $OBJ_PATH/*.o || fail
    rm -f $OBJ_PATH/*.SYMDEF* || fail

    (
        cd $OBJ_PATH; ar -x ../../$NAME || fail;
            for FILE in *.o; do
                NEW_FILE="${NAME}_${FILE}"
                mv $FILE $NEW_FILE || fail
            done
    );
    $AR_UTILITY crus $OUT_LIB $OBJ_PATH/*.o || fail;
}

 
function wrbase() {
    export GYP_DEFINES="build_with_libjingle=1 build_with_chromium=0 libjingle_objc=1 build_libyuv=1 include_tests=0 build_neon=1 libvpx_build_vp9=0"
    export GYP_GENERATORS="ninja"
}
 
function wrios() {
    wrbase
    ARCH=$1
    OUTDIR=$2
    export GYP_DEFINES="$GYP_DEFINES OS=ios target_arch=$ARCH"
    export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=$OUTDIR"
    export GYP_CROSSCOMPILE=1
}
 
function buildios() {
    targets="CNG G711 channel_transport libyuv libyuv_neon opus libvpx video_coding_utility webrtc webrtc_base voice_engine audio_coding_module audio_conference_mixer audio_device video_capture_module video_engine_core video_processing video_render_module webrtc_utility audio_processing bitrate_controller bwe_tools_util CNG common_audio common_video G711 G722 iSAC iSACFix iLBC media_file NetEq4 paced_sender PCM16B rbe_components remote_bitrate_estimator rtp_rtcp system_wrappers video_capture_module video_coding_utility video_engine_core video_processing video_render_module voice_engine webrtc_i420 webrtc_opus webrtc_utility webrtc_video_coding webrtc_vp8 webrtc opus isac_neon audio_processing_neon common_audio_neon libjingle_peerconnection_objc libjingle_peerconnection libjingle libjingle_media libjingle_p2p libjingle_sound libsrtp jsoncpp webrtc_common"
    ARCH=$1
    BUILD_DIR=out_ios
    BUILD_DIR_ARCH=$BUILD_DIR/$ARCH

    pushd trunk
    wrios $ARCH $BUILD_DIR_ARCH && gclient runhooks && ninja -C $BUILD_DIR_ARCH/$BUILD_CONFIG-iphoneos $targets || fail

    LIBWEBRTC_LIBS="CNG G711 G722 NetEq4 PCM16B _core_neon_offsets audio_coding_module audio_conference_mixer audio_device audio_processing audio_processing_neon bitrate_controller bwe_tools_util channel_transport common_audio common_audio_neon common_video iLBC iSAC iSACFix isac_neon media_file opus paced_sender rbe_components remote_bitrate_estimator rtp_rtcp system_wrappers video_capture_module video_coding_utility video_engine_core video_processing video_render_module voice_engine vpx vpx_asm_offsets_vp8 vpx_asm_offsets_vpx_scale webrtc webrtc_base webrtc_i420 webrtc_opus webrtc_utility webrtc_video_coding webrtc_vp8 yuv yuv_neon jsoncpp webrtc_common"
    for LIBWEBRTC_LIB in $LIBWEBRTC_LIBS; do
        FILE="$BUILD_DIR_ARCH/$BUILD_CONFIG-iphoneos/lib$LIBWEBRTC_LIB.a"
        unpackAndCombine $FILE $ARM_DEV_DIR/ar $OUT_IOS_DIR/libwebrtc_$ARCH.a
    done

    LIBJINGLE_LIBS="jingle_peerconnection_objc jingle_peerconnection jingle jingle_media jingle_p2p jingle_sound srtp"
    for LIBJINGLE_LIB in $LIBJINGLE_LIBS; do
        FILE="$BUILD_DIR_ARCH/$BUILD_CONFIG-iphoneos/lib$LIBJINGLE_LIB.a"
        unpackAndCombine $FILE $ARM_DEV_DIR/ar $OUT_IOS_DIR/libtalk_$ARCH.a
    done

    popd
}

function wrsim() {
    wrbase
    ARCH=$1
    OUTDIR=$2
    export GYP_DEFINES="$GYP_DEFINES OS=ios target_arch=$ARCH"
    export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=$OUTDIR"
    export GYP_CROSSCOMPILE=1 
}

function buildsim() {
    targets="channel_transport iossim libyuv opus libvpx libvpx_intrinsics_mmx libvpx_intrinsics_sse2 libvpx_intrinsics_ssse3 video_coding_utility webrtc_all libjingle_peerconnection_objc libjingle_peerconnection libjingle libjingle_media libjingle_p2p libjingle_sound nss nspr nss_static libssl nssckbi libsrtp jsoncpp webrtc_common"
    ARCH=$1
    BUILD_DIR=out_sim
    BUILD_DIR_ARCH=$BUILD_DIR/$ARCH

    pushd trunk
    wrsim $ARCH $BUILD_DIR_ARCH && gclient runhooks && ninja -C $BUILD_DIR_ARCH/$BUILD_CONFIG $targets || fail

    LIBWEBRTC_LIBS="CNG G711 G722 NetEq4 PCM16B audio_coding_module audio_conference_mixer audio_device audio_processing audio_processing_sse2 bitrate_controller bwe_tools_util channel_transport common_audio common_audio_sse2 common_video iLBC iSAC iSACFix media_file opus paced_sender rbe_components remote_bitrate_estimator rtp_rtcp system_wrappers video_capture_module video_coding_utility video_engine_core video_processing video_processing_sse2 video_render_module voice_engine vpx vpx_asm_offsets_vp8 vpx_intrinsics_mmx vpx_intrinsics_sse2 vpx_intrinsics_ssse3 webrtc webrtc_base webrtc_i420 webrtc_opus webrtc_utility webrtc_video_coding webrtc_vp8 yuv jsoncpp webrtc_common"
    for LIBWEBRTC_LIB in $LIBWEBRTC_LIBS; do
        FILE="$BUILD_DIR_ARCH/$BUILD_CONFIG/lib$LIBWEBRTC_LIB.a"
        unpackAndCombine $FILE ar $OUT_IOS_DIR/libwebrtc_$ARCH.a        
    done

    LIBJINGLE_LIBS="jingle_peerconnection_objc jingle_peerconnection jingle jingle_media jingle_p2p jingle_sound srtp"
    for LIBJINGLE_LIB in $LIBJINGLE_LIBS; do
        FILE="$BUILD_DIR_ARCH/$BUILD_CONFIG/lib$LIBJINGLE_LIB.a"
        unpackAndCombine $FILE ar $OUT_IOS_DIR/libtalk_$ARCH.a
    done
    popd
}

function lipoficate() {
    pushd trunk/$OUT_IOS_DIR >> /dev/null
        xcrun -sdk iphoneos lipo -create -output libwebrtc.a $(ls | grep libwebrtc) || abort "Lipo failed"
        xcrun -sdk iphoneos lipo -create -output libtalk.a $(ls | grep libtalk) || abort "Lipo failed"        
    popd >> /dev/null
}


buildFramework() {
    # : ${1:?}
    FRAMEWORKDIR=$1
    FAT_LIB_PATH=$2
    FRAMEWORK_NAME=$3    

    VERSION_TYPE=Alpha

    FRAMEWORK_VERSION=A

    FRAMEWORK_CURRENT_VERSION="0.2"
    FRAMEWORK_COMPATIBILITY_VERSION="0.2"

    FRAMEWORK_BUNDLE=$FRAMEWORKDIR/$FRAMEWORK_NAME.framework
    echo "Framework: Building $FRAMEWORK_BUNDLE from $BUILDDIR..."

    rm -rf $FRAMEWORK_BUNDLE

    echo "Framework: Setting up directories..."
    mkdir -p $FRAMEWORK_BUNDLE
    mkdir -p $FRAMEWORK_BUNDLE/Versions
    mkdir -p $FRAMEWORK_BUNDLE/Versions/$FRAMEWORK_VERSION
    mkdir -p $FRAMEWORK_BUNDLE/Versions/$FRAMEWORK_VERSION/Resources
    mkdir -p $FRAMEWORK_BUNDLE/Versions/$FRAMEWORK_VERSION/Headers
    mkdir -p $FRAMEWORK_BUNDLE/Versions/$FRAMEWORK_VERSION/Documentation

    echo "Framework: Creating symlinks..."
    ln -s $FRAMEWORK_VERSION               $FRAMEWORK_BUNDLE/Versions/Current
    ln -s Versions/Current/Headers     $FRAMEWORK_BUNDLE/Headers
    ln -s Versions/Current/Resources       $FRAMEWORK_BUNDLE/Resources
    ln -s Versions/Current/Documentation   $FRAMEWORK_BUNDLE/Documentation
    ln -s Versions/Current/$FRAMEWORK_NAME $FRAMEWORK_BUNDLE/$FRAMEWORK_NAME

    FRAMEWORK_INSTALL_NAME=$FRAMEWORK_BUNDLE/Versions/$FRAMEWORK_VERSION/$FRAMEWORK_NAME

    echo "Framework: Lipoing library $FAT_LIB_PATH into $FRAMEWORK_INSTALL_NAME ..."
    cp $FAT_LIB_PATH $FRAMEWORK_INSTALL_NAME

    echo "Framework: Copying includes..."
    pushd $FRAMEWORK_NAME >> /dev/null
    find . -name '*.h' | cpio -pdm "../$FRAMEWORK_BUNDLE/Headers"
    popd >> /dev/null

    echo "Framework: Creating plist..."
    cat > $FRAMEWORK_BUNDLE/Resources/Info.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>CFBundleDevelopmentRegion</key>
    <string>English</string>
    <key>CFBundleExecutable</key>
    <string>${FRAMEWORK_NAME}</string>
    <key>CFBundleIdentifier</key>
    <string>org.$FRAMEWORK_NAME</string>
    <key>CFBundleInfoDictionaryVersion</key>
    <string>6.0</string>
    <key>CFBundlePackageType</key>
    <string>FMWK</string>
    <key>CFBundleSignature</key>
    <string>????</string>
    <key>CFBundleVersion</key>
    <string>${FRAMEWORK_CURRENT_VERSION}</string>
</dict>
</plist>
EOF
}


export PATH="$PATH":~/source/depot_tools  
export BUILD_CONFIG=Release
IPHONE_OS_PLATFORM_PATH=$(xcrun --sdk iphoneos --show-sdk-platform-path)
ARM_DEV_DIR=$IPHONE_OS_PLATFORM_PATH/Developer/usr/bin
IPHONE_SIMULATOR_PLATFORM_PATH=$(xcrun --sdk iphonesimulator --show-sdk-platform-path)
SIM_DEV_DIR=$IPHONE_SIMULATOR_PLATFORM_PATH/Developer/usr/bin
OUT_IOS_DIR="OUT_IOS_LIB"

fetch || fail

pushd trunk >> /dev/null
    mkdir -p $OUT_IOS_DIR || fail
    rm -f $OUT_IOS_DIR/* || fail
popd >> /dev/null

buildios armv7s || fail
buildios armv7 || fail
buildsim ia32 || fail 


lipoficate
pushd trunk >> /dev/null
    buildFramework "OUT_IOS_FRAMEWORK" "$OUT_IOS_DIR/libwebrtc.a" webrtc
    buildFramework "OUT_IOS_FRAMEWORK" "$OUT_IOS_DIR/libtalk.a" talk    
popd >> /dev/null
